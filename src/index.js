import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import './index.css';
import App from './App';
import rootReducer from './reducers/rootReducer';
import wsService from './services/websocket';

const store = createStore(rootReducer);
wsService.setConnection();

ReactDOM.render(<Provider store={store}>
  <App />
</Provider>, document.getElementById('root'));
