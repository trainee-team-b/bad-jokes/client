import React from 'react';
import { Card, CardContent, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { HOST_CARD } from '../constants/colors';

const useStyles = makeStyles({
  redBack: {
    background: HOST_CARD,
    color: 'white',
    textAlign: 'center',
  },
});

const HostCard = (props) => {
  const classes = useStyles();
  const { cardText } = props;
  return (
    <Card className={classes.redBack}>
      <CardContent>
        <Typography>
          {cardText}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default HostCard;
