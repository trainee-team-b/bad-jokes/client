import React from 'react';
import { Card, CardContent, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { SELECTED_PLAYER_CARD } from '../constants/colors';

const useStyles = makeStyles({
  cardIsSelected: {
    background: SELECTED_PLAYER_CARD,
    textAlign: 'center',
    color: 'white',
  },
  cardIsNotSelected: {
    textAlign: 'center',
  },
});

const PlayersCard = (props) => {
  const classes = useStyles();
  const { cardText, isSelected } = props;
  return (
    <Card className={isSelected ? classes.cardIsSelected : classes.cardIsNotSelected}>
      <CardContent>
        <Typography>
          {cardText}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default PlayersCard;
