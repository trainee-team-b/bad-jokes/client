import React from 'react';
import { AppBar, Toolbar, IconButton } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';

class HeaderAppBar extends React.Component {

  navigateTo = (link) => {
    const { history } = this.props;
    history.push(link);
  }

  render() {
    const { previousPage } = this.props;
    return (
      <AppBar position="absolute">
        <Toolbar variant="dense">
          <IconButton color="inherit" edge="start" onClick={() => this.navigateTo(previousPage)}>
            <KeyboardBackspaceIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    );
  }

}

export default withRouter(HeaderAppBar);
