export const ERROR_MESSAGES = {
  EMPTY_INPUT: 'Это поле обязательно для заполнения',
  ROOM_NAME_CHARACTERS_FAIL: 'Имя комнаты может содержать только цифры и латиницу',
  ROOM_NAME_LENGTH_FAIL: 'Размер имени комнаты - от 6 до 30 символов',
  ROOM_NAME_WHITE_SPACES_FAIL: 'Имя комнаты не должно содержать пробелов',
  EMPTY_PLACEHOLDER: ' ',
};