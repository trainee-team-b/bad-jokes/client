export const ROOM_NAME_LENGTH_PATTERN = /^.{6,30}$/img;
export const ROOM_NAME_CHAR_PATTERN = /^[\da-z]+$/img;
export const ROOM_NAME_SPACES_PATTERN = /\s/img;
export const NOT_EMPTY_INPUT_PATTERN = /.+/img;