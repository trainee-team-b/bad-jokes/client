import React from 'react';
import { connect } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';

import HostCard from '../components/HostCard';
import PlayerCard from '../components/PlayerCard';

import { ROLES } from '../constants/roles';
import { ACCEPT_CARD_BUTTON } from '../constants/colors';

const styles = {
  greenBack: {
    background: ACCEPT_CARD_BUTTON,
    color: 'white',
  },
  pageMargin: {
    marginTop: 8,
  },
};

class Game extends React.Component {

  state = {
    selectedCardId: null,
  }

  selectCard = (cardId) => {
    if (cardId !== this.state.selectedCardId) {
      this.setState({ selectedCardId: cardId });
      return;
    }
    this.setState({ selectedCardId: null });
  }

  submitCard = () => { }

  render() {
    const {
      playerState: {
        role,
        cards,
      },
      gameState: { currentHostCard },
      classes,
    } = this.props;
    const { selectedCardId } = this.state;

    const playerBody = cards.map((card, index) => {
      return (
        <Grid item xs={10} onClick={() => this.selectCard(card.id)} key={index}>
          <PlayerCard cardText={card.text} isSelected={card.id === selectedCardId ? true : false} />
        </Grid>
      );
    });

    const hostBody = null;


    return (
      <div className={classes.pageMargin}>

        <Grid
          container
          spacing={2}
          justify="center"
          alignItems="center"
        >

          <Grid item xs={11}>
            <HostCard cardText={currentHostCard.text} />
          </Grid>

          {role === ROLES.HOST ? hostBody : playerBody}

          <Grid item>
            <Button
              className={classes.greenBack}
              variant="contained"
              onClick={this.submitCard}
              disabled={!Boolean(this.state.selectedCardId)}
            >Подтвердить</Button>
          </Grid>
        </Grid>

      </div>
    );
  }
}

const mapStateToProps = (globalState) => {
  return {
    playerState: globalState.playerState,
    gameState: globalState.gameState,
  };
};

export default withStyles(styles)(connect(mapStateToProps)(Game));
