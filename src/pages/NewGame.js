import React from 'react';
import { Button, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';

import HeaderAppBar from '../components/HeaderAppBar';

const styles = {
  seventyScreenHeight: {
    height: '70vh',
  },
};

class NewGame extends React.Component {

  navigateTo = (link) => {
    const { history } = this.props;
    history.push(link);
  }

  render() {
    const { classes } = this.props;
    return (
      <div>

        <HeaderAppBar previousPage={'/'} />

        <Grid
          className={classes.seventyScreenHeight}
          container
          direction="column"
          justify="center"
          alignItems="center"
          spacing={4}
        >
          <Grid item>
            <Button color="primary" variant="contained" onClick={() => this.navigateTo('/prelobby/host')}>Начать игру</Button>
          </Grid>

          <Grid item>
            <Button color="primary" variant="contained" onClick={() => this.navigateTo('/prelobby/connect')}>Присоединиться к игре</Button>
          </Grid>

        </Grid>

      </div>
    );
  }

}

export default withStyles(styles)(NewGame);
