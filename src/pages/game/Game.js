import React from 'react';
import { Button } from 'antd';

import apiService from '../../services/api/api';
import Todo from '../../components/Todo';

class Game extends React.Component {
  state = {
    cardContent: {
      completed: true,
    },
  };

  getTodo = async () => {
    const { data } = await apiService.todo.getTodo(1);

    this.setState({ cardContent: data });
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.getTodo}>
          Fetch user
        </Button>

        <Todo todo={this.state.cardContent} />
      </div>
    );
  }
}

export default Game;
