import React from 'react';

import { TextField, Button, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';

import HeaderAppBar from '../components/HeaderAppBar';
import { ROOM_NAME_LENGTH_PATTERN, ROOM_NAME_CHAR_PATTERN, ROOM_NAME_SPACES_PATTERN, NOT_EMPTY_INPUT_PATTERN } from '../constants/inputRules';
import { ERROR_MESSAGES } from '../constants/inputErrorMessages';

const styles = {
  seventyScreenHeight: {
    height: '70vh',
  },
};

class PreLobby extends React.Component {
  state = {
    inputsInfo: {
      nickName: '',
      roomName: '',
    },
    errorMessages: {
      nicknameErrorMessage: ERROR_MESSAGES.EMPTY_PLACEHOLDER,
      roomNameErrorMessage: ERROR_MESSAGES.EMPTY_PLACEHOLDER,
    },
  }

  setNicknameInfo = ({ target: { value: nickName } }) => {
    this.setState({
      inputsInfo: {
        ...this.state.inputsInfo,
        nickName,
      },
    });
  }

  setRoomNameInfo = ({ target: { value: roomName } }) => {
    this.setState({
      inputsInfo: {
        ...this.state.inputsInfo,
        roomName,
      },
    });
  }

  hostLobby = () => {
    const { history } = this.props;
    history.push('/game');
  }

  connectToLobby = () => {
    const { history } = this.props;
    history.push('/game');
  }

  checkRoomName = () => {
    const { roomName } = this.state.inputsInfo;

    const roomNameIsNotEmpty = new RegExp(NOT_EMPTY_INPUT_PATTERN).test(roomName);
    const roomNameContainsSpaces = new RegExp(ROOM_NAME_SPACES_PATTERN).test(roomName);
    const roomNameCharIsCorrect = new RegExp(ROOM_NAME_CHAR_PATTERN).test(roomName);
    const roomNameLengthIsCorrect = new RegExp(ROOM_NAME_LENGTH_PATTERN).test(roomName);

    if (!roomNameIsNotEmpty) {
      return ERROR_MESSAGES.EMPTY_INPUT;
    };

    if (roomNameContainsSpaces) {
      return ERROR_MESSAGES.ROOM_NAME_WHITE_SPACES_FAIL;
    };

    if (!roomNameCharIsCorrect) {
      return ERROR_MESSAGES.ROOM_NAME_CHARACTERS_FAIL;
    };

    if (!roomNameLengthIsCorrect) {
      return ERROR_MESSAGES.ROOM_NAME_LENGTH_FAIL;
    };

    return ERROR_MESSAGES.EMPTY_PLACEHOLDER;
  }

  checkNickname = () => {
    const { nickName } = this.state.inputsInfo;

    return new RegExp(NOT_EMPTY_INPUT_PATTERN).test(nickName) ? ERROR_MESSAGES.EMPTY_PLACEHOLDER : ERROR_MESSAGES.EMPTY_INPUT;
  }

  setErrorMessages = ({ nicknameErrorMessage, roomNameErrorMessage }) => {
    const newErrorMessages = {
      nicknameErrorMessage: nicknameErrorMessage || this.state.errorMessages.nicknameErrorMessage,
      roomNameErrorMessage: roomNameErrorMessage || this.state.errorMessages.roomNameErrorMessage,
    };

    this.setState({
      errorMessages: newErrorMessages,
    });
  }

  render() {
    const {
      classes,
      match: {
        params: { action },
      },
    } = this.props;
    const { roomNameErrorMessage, nicknameErrorMessage } = this.state.errorMessages;

    return (
      <div>

        <HeaderAppBar previousPage={'/newgame'} />

        <form onSubmit={(event) => {
          event.preventDefault();
          const nicknameErrorMessage = this.checkNickname();
          const roomNameErrorMessage = this.checkRoomName();

          if (!Boolean(roomNameErrorMessage.trim()) && !Boolean(nicknameErrorMessage.trim())) {
            if (action === 'host') {
              this.hostLobby();
            }
            this.connectToLobby();
          }

          this.setErrorMessages({ nicknameErrorMessage, roomNameErrorMessage });
        }}>

          <Grid
            className={classes.seventyScreenHeight}
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={4}
          >
            <Grid item>
              <TextField
                className={classes.inputs}
                onChange={this.setRoomNameInfo}
                onBlur={() => this.setErrorMessages({ roomNameErrorMessage: this.checkRoomName() })}
                error={Boolean(roomNameErrorMessage.trim())}
                label="Номер комнаты"
                helperText={roomNameErrorMessage}
              />
            </Grid>

            <Grid item>
              <TextField
                className={classes.inputs}
                onChange={this.setNicknameInfo}
                onBlur={() => this.setErrorMessages({ nicknameErrorMessage: this.checkNickname() })}
                error={Boolean(nicknameErrorMessage.trim())}
                label="Ваш никнейм"
                helperText={nicknameErrorMessage}
              />
            </Grid>

            <Grid item>
              <Button color="primary" variant="contained" type="submit">{action === 'host' ? 'Подтвердить' : 'Присоединиться'}</Button>
            </Grid>
          </Grid>

        </form>

      </div >
    );
  }

}

export default withStyles(styles)(PreLobby);
