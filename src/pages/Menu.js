import React from 'react';
import { Button, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';

const styles = {
  seventyScreenHeight: {
    height: '70vh',
  },
};

class Menu extends React.Component {

  navigateTo = (link) => {
    const { history } = this.props;
    history.push(link);
  }

  render() {
    const { classes } = this.props;
    return (
      <div>

        <Grid
          className={classes.seventyScreenHeight}
          container
          direction="column"
          justify="center"
          alignItems="center"
        >

          <Grid item color="primary">
            <Button color="primary" variant="contained" onClick={() => this.navigateTo('/newgame')}>Новая игра</Button>
          </Grid>

        </Grid>

      </div>
    );
  }
}

export default withStyles(styles)(Menu);
