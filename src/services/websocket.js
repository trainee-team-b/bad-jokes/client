import io from 'socket.io-client';
import { rootUrl } from '../constants/api';


class WebSocketService {

  socket = null;

  setConnection = () => {
    this.socket = io.connect(rootUrl);
  }

}

export default new WebSocketService();
