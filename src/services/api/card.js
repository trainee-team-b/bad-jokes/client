import axios from 'axios';

const root = 'card';

const getCardById = (id) => axios.get(`${root}/${id}`);

const card = { getCardById };

export default card;
