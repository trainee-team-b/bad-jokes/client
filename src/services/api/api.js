import axios from 'axios';

import { rootUrl } from '../../constants/api';
import card from './card';

axios.defaults.baseURL = rootUrl;

export default { card };
