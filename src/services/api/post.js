import axios from 'axios';

const root = 'posts';

const getAllPosts = () => axios.get(`${root}`);

const post = { getAllPosts };

export default post;
