import { ROLES } from '../constants/roles';

export const players = [
  {
    id: 1,
    nickname: 'Galaxy collapse',
    role: ROLES.PLAYER,
  },
  {
    id: 2,
    nickname: 'Temoncher',
    role: ROLES.PLAYER,
  },
  {
    id: 3,
    nickname: 'Akkuratnenko',
    role: ROLES.HOST,
  },
  {
    id: 4,
    nickname: 'Chessny chelovek',
    role: ROLES.PLAYER,
  },
];