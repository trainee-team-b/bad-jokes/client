import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Menu from './pages/Menu';
import NewGame from './pages/NewGame';
import PreLobby from './pages/PreLobby';
import Game from './pages/Game';

class App extends React.Component {

  render() {
    return (
      <div>

        <Router>
          <Switch>
            <Route exact path="/" component={Menu} />
            <Route exact path="/newgame" component={NewGame} />
            <Route exact path="/prelobby/:action" component={PreLobby} />
            <Route exact path="/game" component={Game} />
          </Switch>
        </Router>

      </div>
    );
  }
}

export default App;
