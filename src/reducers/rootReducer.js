import { combineReducers } from 'redux';

import cardsReducer from './cardsReducer';
import gameReducer from './gameReducer';
import playerRuducer from './playerRuducer';

const rootReducer = combineReducers({
  cardsState: cardsReducer,
  gameState: gameReducer,
  playerState: playerRuducer,
});

export default rootReducer;