import { players } from '../mockups/players';
import { redCards } from '../mockups/cards';

const getRedCard = () => redCards[0];

const initState = {
  players,
  currentHostCard: getRedCard(),
};

const gameReducer = (state = initState, action) => {
  return state;
};

export default gameReducer;