import { players } from '../mockups/players';
import { whiteCards } from '../mockups/cards';

const getSixWhiteCards = () => whiteCards.slice(0, 6);

const initState = {
  id: players[0].id,
  role: players[0].role,
  nickname: players[0].nickname,
  cards: getSixWhiteCards(),
};

const playersReducer = (state = initState, action) => {
  return state;
};

export default playersReducer;